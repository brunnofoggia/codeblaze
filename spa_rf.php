<?php

namespace codeigniter\CodeBlaze;

/**
 * SPA Restful Trait
 *
 * This trait is a additional pack for Controllers Restful
 * It will provide the basics for CRUD and Restful methods
 *
 * @package     CodeBlaze
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/codeblaze
 */
trait spa_rf {

    use CtrlBasis;
    
    protected function getMethodRedirToAfterSave($result) {
        $loadFormAfterSave = $this->getAttr('loadFormAfterSave');
        return (empty($result) ? 'index' : (empty($loadFormAfterSave) ? 'list' : 'index/'.$result));
    }
    
    protected function getMethodRedirToAfterRemove($result) {
        return 'list';
    }
    
    public function list_get() {
        return call_user_func_array([$this, 'index'], func_get_args());
    }
    
    public function view_get() {
        return call_user_func_array([$this, 'view'], func_get_args());
    }
    
    public function index_get() {
        return call_user_func_array([$this, 'form'], func_get_args());
    }
    
    public function form_get() {
        return call_user_func_array([$this, 'form'], func_get_args());
    }
    
    public function index_post() {
        return call_user_func_array([$this, 'save'], func_get_args());
    }
    
    public function index_put() {
        return call_user_func_array([$this, 'save'], func_get_args());
    }
    
    public function index_delete() {
        return call_user_func_array([$this, 'remove'], func_get_args());
    }
    
    public function get_get() {
        header("Content-Type: application/json");
        return call_user_func_array([$this, 'get'], func_get_args());
    }
    
    public function create_post() {
        return call_user_func_array([$this, 'create'], func_get_args());
    }
    
    public function update_post() {
        return call_user_func_array([$this, 'update'], func_get_args());
    }
    
    /**
     * used to integrate with recordcontrol
     * @param type $id
     */
    protected function buildRecordControlShareList($id) {
        $lists = $this->Model->buildRecordControlShareList($id);
        foreach($lists as $x => $y) $this->set($x, $y);
        
        $this->Model->loadModel('\Model\Dynamic');
    }
}
