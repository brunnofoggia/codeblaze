# Controller

## Properties avaiable for customization

    class Sample extends CI_Controller {
        use \codeigniter\CodeBlaze\Controller;
        // path for master view
        public $layout = 'layout/default';
        // array with messages for save, delete, invalid token
        public $errorMsgs = [
            'save' => '',
            'delete' => '',
            'token' => ''
        ];
        // Token Security
        // Defines if token control for post requests will be applied
        public $tokenSecurity = true;
        // Key Name for GET global item
        public $tokenKeyName = 'token';
        // used to encrypt token
        public $secretKey = 'CodeBlaze';
    }

## Methods for rendering views and partial views

    class Sample extends CI_Controller {
        /**
         * Render a view - with variables set for keys into $data variable - into layout view
         */
        public function home() {
            $data = ['msg'=>'Type what do you want to search for'];
            $this->render('sample/home', $data);
        }
        
        /**
         * Render a view without layout view
         */
        public function search() {
            $this->renderPartial('sample/search');
        }
    }

## Default methods
- Rest methods: get, create, update, delete
- CRUD methods: index, view, form, save
- CRUD retrieve methods: indexList, viewItem, formItem - These 3 are responsible for providing the main data for the correspondent methods index, view and form)
- CRUD retrieve lists methods: indexLists, viewLists, formLists - These 3 are responsible for providing auxiliar data for the correspondent methods index, view and form)

This methods are avaiable by default.
The only thing needed is to load the according model into "Model" property, as the example that follows.

    class Sample extends CI_Controller {
        public function __construct() {
            parent::__construct();
            $this->load->model('Sample_model', 'Model');
        }
    }

For more details take a little look at Controller.php