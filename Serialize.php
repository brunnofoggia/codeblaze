<?php

namespace codeigniter\CodeBlaze;

trait Serialize {
    use \DataBoomer\Session;

    /**
     * A constructor executing unserialize method is needed to get it working on codeigniter
     */
    public abstract function __construct();
    
    /**
     * Restore data by UID from SESSION
     * @param string $uid
     */
    public static function restoreData($uid) {
        $ci = \get_instance();
        $ci->load->library('session');
        return $ci->session->userdata(static::dataName($uid)) ? unserialize($ci->session->userdata(static::dataName($uid))) : null;
    }

    /**
     * Store data into SESSION named by uid
     * @param string $uid
     */
    public static function storeData($uid, $data) {
        $ci = \get_instance();
        $ci->load->library('session');
        
        $ci->session->set_userdata(static::dataName($uid), $data);
    }

    /**
     * Clear Session based on uid
     * @param string $uid
     */
    public static function clearStoredData($uid) {
        $ci = \get_instance();
        $ci->load->library('session');
        $ci->session->unset_userdata(static::dataName($uid));
    }
    
    public function generateUID() {
        $ci = \get_instance();
        $ci->load->library('session');
        
        return session_id();
    }

    /**
     * Add a item and value into data
     * And for codeigniter serializes it already
     * @param string $key
     * @param mixed $value
     */
    public function addData($key, $value, $stack = null) {
        $this->_addData($key, $value, $stack);
        $this->serialize();
    }

}
