<?php

namespace codeigniter\CodeBlaze;

/**
 * Model Trait
 *
 * This trait is a additional pack for Models
 * It will provide the basics for read/persist relational data quick and easy
 *
 * @package     CodeBlaze
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/codeblaze
 */
trait Model {

    use \HBasis {
        \HBasis::loadModelInstance as _loadModelInstance;
    }
    
    /**
     * Executes Raw Queries
     * @param string $sql
     * @return object
     */
    public function query($sql) {
        $query = $this->db->query($sql);
        return $query;
    }

    /**
     * Run routine under a Transaction
     * @param type $callback
     * @param mixed $parameter [optional] Zero or more parameters to be passed to the callback.
     * @return boolean
     */
    public function transact($callback) {
        if($this->getGlobal('transaction_running')) {
            return call_user_func_array($callback, array_slice(func_get_args(), 1));
        }
        
        $this->setGlobal('transaction_running', true);
        $this->db->trans_begin();
        $result = call_user_func_array($callback, array_slice(func_get_args(), 1));
        
        if($this->db->trans_status()) {
            $this->db->trans_commit();
        } else {
            $result = false;
            \error_log($this->db->error()['message'] . ' query: '.$this->db->last_query(), 0);
            $this->db->trans_rollback();
        }
        $this->setGlobal('transaction_running', false);
        return $result;
    }

    /**
     * Runs update on data under conditions
     * @param conditions for update
     * @param data to be updated
     * @access public
     */
    public function _updateAll($conditions, $formattedData) {
        $result = $this->db->update($this->getAttr('table'), $formattedData, $conditions);
        return $result;
    }

    /**
     * Delete rows
     * @param conditions group of filters
     * @access public
     */
    public function _deleteAll($conditions = []) {
        $query = $this->getQueryBuilder();
        $this->where($query, $conditions);
        $query->delete($this->table);

        return $query;
    }

    /**
     * Add conditions to query recursively diff arrays from common values
     * @param $query
     * @param $conditions
     */
    public function where($query, $conditions) {
        $namedConditions = $this->exchangeListAliases($conditions);
        foreach ($namedConditions as $field => $value) {
            if (!is_array($value)) {
                !is_int($field) ? $query->where($field, $value) : $query->where($value, null, false);
                continue;
            }
            $query->where_in($field, $value);
        }
    }

    /**
     * Order results
     * @param $query
     * @param $conditions
     */
    public function orderby($query, $orderby) {
        $query->order_by($orderby);
    }

    /**
     * Returns query builder instance
     */
    public function getQueryBuilder() {
        return $this->db;
    }
    
    /**
     * Load model
     * @param string $model
     * @return object instance
     */
    public function loadModelInstance($model) {
        if(strpos($model, '\\')===false && strpos($model, '_model')!==false) {
            $ci = \get_instance();
            $modelShortName = substr($model, 0, strpos($model, '_model'));
            if (!(@$this->model[$modelShortName]) || !is_object(@$this->model[$modelShortName])) {
                $ci->load->model($model);
                $this->model[$modelShortName] = new $model;
            }
        } else {
            $modelShortName = \Crush\Basic::getClassShortName($model);
            $this->_loadModelInstance($model);
        }

        return $this->model[$modelShortName];
    }

    /**
     * Trick to paginate easily with mysql
     * @param $query
     * @param $limit number of rows
     * @param $page use page numbers 0, 1, 2, 3, ...
     */
    public function limit($query, $limit, $page) {
        if (strpos($this->db->dbdriver, 'mysql') !== false) {
            $page = $page === 0 ? 0 : $page * $limit;
        }

        $query->limit($limit, $page);
    }

    /**
     * Fetch results
     * @param object $query
     * @access public
     */
    public function fetchAll($query) {
        return $query->get()->result_array();
    }

    /**
     * Affected rows
     * @param object $query
     * @access public
     */
    public function affectedRows($query) {
        return $this->db->affected_rows();
    }

    /**
     * Last Inserted Id
     * @param object $query
     * @access public
     */
    public function lastInsertId($query) {
        return $this->db->insert_id();
    }

    /**
     * Quote string value
     * @param string $value
     * @return string
     */
    public function quote($value) {
        if (is_array($value)) {
            $self = $this;
            return implode(',', array_map(function($value) use ($self) {
                        return $self->quote($value);
                    }, $value));
        }

        if (preg_match('/\D/', str_replace('.', '', $value)) || $this->getAttr('quoteAlways')) {
            $value = $this->db->escape($value);
        }
        return $value;
    }

    /**
     * Quote identifier
     * @param string $field
     * @return string
     */
    public function quoteField($field) {
        if (preg_match('/^([A-Za-z0-9-_])+$/', $field)) {
            $field = $this->db->protect_identifiers($field);
        }
        return $field;
    }

}

define(__NAMESPACE__ . '\BELONGSTO', \HBasis\BELONGSTO);
define(__NAMESPACE__ . '\HASMANY', \HBasis\HASMANY);
define(__NAMESPACE__ . '\HASANDBELONGSTOMANY', \HBasis\HASANDBELONGSTOMANY);
define(__NAMESPACE__ . '\NORELATED', \HBasis\NORELATED);