<?php

require "vendor/autoload.php";

date_default_timezone_set("America/Sao_Paulo");

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$config = parse_ini_file("config.ini", true);
$docs = new \Scroll\Slim\Core($config, $app);

$app->map(['GET'], '/docs' . '/{name}', function ($request, $response, $args) use ($app, $docs) {
                    $args['name'] = 'docs/'.$args['name'];
                    $docs->openFileByName($args, $request->getUri()->getBaseUrl());
                })->setName('openFileByName');


ob_start();
$app->run();
$contents = ob_get_contents();
@ob_end_clean();
                
echo str_replace('.md', '', $contents);