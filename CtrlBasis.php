<?php

namespace codeigniter\CodeBlaze;

/**
 * Controller Basis Trait
 *
 * This trait is a additional pack for Controllers
 * It will provide the basics for CRUD and Rest methods
 *
 * @package     CodeBlaze
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/codeblaze
 */
trait CtrlBasis {

    use \DarkTrait;

    /**
     * Defines default values for undeclared class properties
     * Array keys represents names of the possible properties that could be declared and customized
     */
    protected $attrDefaults = [
        'layout' => 'layout/index',
        'errorMsgs' => [
            'notFound' => 'Record not found.',
            'save' => 'System Failure. Try again.',
            'saveDenied' => 'Permission denied to change this record.',
            'delete' => 'Cannot delete this.'
        ],
        'infoMsgs' => [
            'created' => 'New registry created successfully',
            'updated' => 'Registry updated successfully'
        ],
        // defines where to redirect after save. index or edit form
        'loadFormAfterSave' => 0,
        // Key Name for SESSION global item
        'sessionKey' => 'CodeBlaze'
    ];

    /**
     * Holds data that will be send to a view
     */
    protected $viewData = [];

    /**
     * Render view files
     * @param view path
     * @param data variables sent for view context
     * @param layout allows to disable/enable layout or change it by sending its path
     * @access protected
     */
    protected function render($view, $data = [], $layout = true) {
        empty($view) && ($view = $this->router->fetch_method());
        $this->load->helper('url');

        $content = $this->renderPartial($view, array_merge($data, $this->viewData), true);
        if (!empty($layout) && !$this->input->is_ajax_request()) {
            if (!is_string($layout)) {
                $layout = $this->getAttr('layout');
            }

            $content = $this->renderLayout($layout, ['content' => $content]);
        }

        echo $content;
    }

    /**
     * Record data to send when load a view
     * @param name
     * @param value
     * @access public
     */
    protected function set($name, $value) {
        $this->viewData[$name] = $value;
    }

    /**
     * Render view files without layout
     * @param view path
     * @param data variables sent for view context
     * @param return defines if view html will be printed or returned
     * @access protected
     */
    protected function renderPartial($view, $data = [], $return = false) {
        $html = $this->load->view($view, $data, true);
        if ($return) {
            return $html;
        }
        echo $html;
    }

    /**
     * Render layout view
     * @param layout path
     * @param data variables sent for view context
     * @access protected
     */
    protected function renderLayout($layout, $data = []) {
        return $this->load->view($layout, $data, true);
    }

    /**
     * Get row(s) in table
     * @param id primaryKey to a specific row or nothing to get them all
     * @param limit use it to paginate
     * @param page use it to paginate
     * @param return defines if result will returned or dispatched as json
     * @access public
     */
    public function get($id = 0, $limit = null, $page = null, $return = false) {
        empty($id) && ($id = null);
        $result = (array) ($id ? $this->Model->get($id) : $this->Model->find([], $limit, $page));
        return $return ? $result : print(json_encode($result));
    }

    /**
     * Create a row into the table using post data
     * @access protected
     */
    protected function create() {
        return $this->Model->create($this->input->post());
    }

    /**
     * Update a row into the table using post data
     * @param id primaryKey to a specific row
     * @access protected
     */
    protected function update($id) {
        return $this->Model->update($id, $this->input->post());
    }

    /**
     * Delete a row into the table
     * @param id primaryKey to a specific row
     * @access public
     */
    public function remove($id = NULL) {
        empty($id) && !empty($_REQUEST['id']) && ($id = $_REQUEST['id']);
        $this->load->helper('url');
        $this->load->library('session');

        $result = !empty($id) ? $this->Model->remove($id) : false;

        $redirToMethod = $this->getMethodRedirToAfterRemove($result);
        $redirectUrl = !$this->input->is_ajax_request() || !empty($result) ? (base_url(((string) $this->router->directory) . $this->router->fetch_class() . '/' . $redirToMethod)) : '';

        $msg = empty($result) ? $this->getAttr('errorMsgs')['delete'] : '';
        return $this->sendMsg(!empty($result), $redirectUrl, $msg, $result);
    }

    /**
     * Render form view to add/edit a row
     * @param id primaryKey to a specific row
     * @param data variables sent for view context
     * @param view path
     * @access public
     */
    public function form($id = null, Array $data = []) {
        empty($id) && !empty($_REQUEST['id']) && ($id = $_REQUEST['id']);
        $this->load->library('session');

        $formItemMethod = $this->buildListMethodName('', '', 'Item');
        $formCollsMethod = $this->buildListMethodName('', '', 'Collections');
        
        $item = empty($id) ? [] : (method_exists($this, $formItemMethod) ? $this->{$formItemMethod}($id, $data) : $this->get($id, null, null, true));
        method_exists($this, $formCollsMethod) && $this->{$formCollsMethod}($id, $data, $item);
        
        if(!empty($id) && (empty($item) || empty($item[$this->Model->getAttr('primaryKey')]))){
            $this->sendMsg(false, '', $this->getAttr('errorMsgs')['notFound']);
        }
        
        $error = $this->session->flashdata('error');
        $msg = $this->session->flashdata('msg');
        $this->render($this->getViewPath(''), array_merge(['item' => $item, 'error' => $error, 'msg' => $msg], $data));
    }

    /**
     * Save a row and dispatch result according to request type
     * @param id primaryKey to a specific row
     * @access public
     */
    public function save($id = null, $return = false) {
        empty($id) && !empty($_REQUEST['id']) && ($id = $_REQUEST['id']);
        $this->load->helper('url');
        $this->load->library('session');

        if(!empty($id) && method_exists($this->Model, 'canAccessRow') && !$this->Model->canAccessRow($id, \HiMax\RECORDCONTROL_WRITE)) {
            return $this->sendMsg(false, false, $this->getAttr('errorMsgs')['saveDenied']);
        }
        $result = $id ? $this->update($id) : $this->create();

        $redirToMethod = $this->getMethodRedirToAfterSave($result);
        $redirectUrl = !$this->input->is_ajax_request() || !empty($result) ? (base_url(((string) $this->router->directory) . $this->router->fetch_class()) . '/' . $redirToMethod) : '';
        
        $msg = empty($result) ? ($this->getAttr('errorMsgs')['save']) : 
            (empty($id) ? $this->getAttr('infoMsgs')['created'] : $this->getAttr('infoMsgs')['updated']);

        return $return ? [!empty($result), $redirectUrl, $msg, $result] : $this->sendMsg(!empty($result), $redirectUrl, $msg, $result);
    }

    /**
     * Render "view" for a row
     * @param id primaryKey to a specific row
     * @access public
     */
    public function view($id) {
        empty($id) && !empty($_REQUEST['id']) && ($id = $_REQUEST['id']);
        
        $viewItemMethod = $this->buildListMethodName('', '', 'Item');
        $viewCollsMethod = $this->buildListMethodName('', '', 'Collections');
        
        $result = method_exists($this, $viewItemMethod) ? $this->{$viewItemMethod}($id) : $this->get($id, null, null, true);
        method_exists($this, $viewCollsMethod) && $this->{$viewCollsMethod}($id, $result);

        $this->render($this->getViewPath(''), ['item' => $result]);
    }

    /**
     * Render list of rows
     * @param limit use it to paginate
     * @param page use it to paginate
     * @access public
     */
    public function index($limit = null, $page = null) {
        $this->load->library('session');

        $indexMainCollectionMethod = $this->buildListMethodName('', '', 'Items');
        $indexCollsMethod = $this->buildListMethodName('', '', 'Collections');
        
        $results = method_exists($this, $indexMainCollectionMethod) ? $this->{$indexMainCollectionMethod}($limit, $page) : $this->get(null, $limit, $page, true);
        method_exists($this, $indexCollsMethod) && $this->{$indexCollsMethod}($results);

        $error = $this->session->flashdata('error');
        $msg = $this->session->flashdata('msg');
        $this->render($this->getViewPath(''), ['list' => $results, 'error' => $error, 'msg' => $msg]);
    }
    
    /**
     * Build the path to a view based on router data (directory, class and method called)
     * @param type $calledMethod name of the CodeBlaze\Controller method that's rendering the view
     * @return string
     */
    protected function getViewPath($calledMethod = '') {
        empty($calledMethod) && ($calledMethod = $this->router->fetch_method());
        $viewPath = ((string) $this->router->directory) . $this->router->fetch_class() . '/' . $calledMethod;
        
        return $viewPath;
    }
    
    /**
     * Just concatenates the strings
     * @param type $calledMethod name of the CodeBlaze\Controller method that's rendering the view
     * @param type $prefix
     * @param type $suffix
     * @return string
     */
    protected function buildListMethodName($calledMethod, $prefix = '', $suffix = '') {
        empty($calledMethod) && ($calledMethod = $this->router->fetch_method());
        return $prefix . $calledMethod . $suffix;
    }

    /**
     * Redirect user to the given destination and set flash message or send it by json depending on the request type
     * @param bool $status
     * @param string $redirectUrl
     * @param string $msg if !status will be consider as an error otherwise an info msg
     * @param mixed $results
     */
    public function sendMsg($status, $redirectUrl, $msg, $results = '') {
        $this->load->library('session');
        $this->load->helper('url');

        $msgType = !$status ? 'error' : 'info';
        $msg && !$this->input->is_ajax_request() ? $this->session->set_flashdata($msgType, $msg) : null;
        
        if(!$this->input->is_ajax_request()) {
            return (!empty($redirectUrl) && \redirect($redirectUrl));
        }
        $json = ['status' => $status, 'msgType' => $msgType, 'msgText' => $msg, 'results' => $results];
        !empty($redirectUrl) && ($json['load'] = $redirectUrl);
        $this->sendJsonMsg($json);
    
    }
    
    /**
     * Dispatch result by json
     * - Created to open for customization the array sent
     * @param type $data
     */
    public function sendJsonMsg($data) {
        $json = [];
        foreach(['status', 'load', 'results'] as $key) {
            array_key_exists($key, $data) && ($json[$key] = $data[$key]);
        }
        $json[$data['msgType']] = $data['msgText'];

        print(json_encode($json));
    }

    /**
     * Redirect to the given url considering request type
     * @param string $url
     * @param bool $baseUrl
     */
    public function redirect($url, $baseUrl = true) {
        $this->load->helper('url');
        
        $baseUrl && $url = \base_url((substr($url, 0, 1)!=='/' ? '/' : '').$url);
        !$this->input->is_ajax_request() ? \redirect($url) : print(json_encode(['load' => $url]));
    }

}
