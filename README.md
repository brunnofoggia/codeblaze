# CodeBlaze

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

Toolkit to build web applications faster using purely CodeIgniter. It makes able 
to develop projects much faster than you could if you were writing code using 
native features by providing a rich set of functionalities for commonly needed tasks.

## Version notice

This works for **CodeIgniter3.x**

## What this plugin provides?

### Features for Controller
- Methods for rendering views and partial views
- Rest methods: get, create, update, delete
- CRUD methods: index, view, form, save
- Properties available for customization: layout, errorMsgs[delete, save]

### Features for Model
- Persist methods - save, update, create, delete, deleteAll, transact
- Retrieve methods - get, find, exists
- Persist related data methods - saveBelongsTo, saveHasMany, saveHasAndBelongsToMany
- Retrieve related data methods - getRelated, getBelongsTo, getHasMany, getHasAndBelongsToMany
- Properties available for customization: table, primaryKey, foreignKeys[], fieldsFormat[], recursive

## Installation

You should install this with composer. Read the [VCS repo docs](https://getcomposer.org/doc/05-repositories.md#vcs).
A initial composer.json would looks like:

    {
      "repositories": [
          {
              "type": "vcs",
              "url": "https://bitbucket.org/brunnofoggia/CodeBlaze.git"
          },
          {
              "type": "vcs",
              "url": "https://bitbucket.org/brunnofoggia/h-basis.git"
          }
      ],
      "require": {
          "codeigniter/framework": "3.*",
          "codeigniter/CodeBlaze": "1.*"
      }
    }

## Usage

Its just necessary to import it into classes that would use it.

Controller example:

    class Sample extends CI_Controller {
        use \codeigniter\CodeBlaze\Controller;
    }

Model example:

    class Sample_model extends CI_Model {
        use \codeigniter\CodeBlaze\Model;
        protected $table = 'sample';
    }

### Documentation
- [Controller](docs/Controller.md)
- [Model](docs/Model.md)