<?php

namespace codeigniter\CodeBlaze;

/**
 * Controller Trait
 *
 * This trait is a additional pack for Controllers
 * It will provide the basics for CRUD and Rest methods
 *
 * @package     CodeBlaze
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/codeblaze
 */
trait Controller {

    use CtrlBasis;
    
    public function delete($id = NULL) {
        return $this->remove($id);
    }
    
    protected function getMethodRedirToAfterSave($result) {
        $loadFormAfterSave = $this->getAttr('loadFormAfterSave');
        return (empty($result) ? 'form' : (empty($loadFormAfterSave) ? 'index' : 'form/'.$result));
    }
    
    protected function getMethodRedirToAfterRemove($result) {
        return 'index';
    }
    
    protected function buildListMethodName($calledMethod, $prefix = '', $suffix = '') {
        return $prefix . $calledMethod . $suffix;
    }
}
